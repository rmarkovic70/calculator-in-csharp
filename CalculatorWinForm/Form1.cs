﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculatorWinForm
{
    public partial class Form1 : Form
    {
        double prvi, drugi, treci, rez;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void zbroji_button_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out prvi))
            {
                MessageBox.Show("Krivi unos prvog broja!", "GRESKA");
            }
            if (!double.TryParse(textBox2.Text, out drugi))
            {
                MessageBox.Show("Krivi unos drugog broja!", "GRESKA");
            }
            rez = prvi + drugi;
            rezultat_label.Text = rez.ToString();
        }

        private void oduzmi_button_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out prvi))
            {
                MessageBox.Show("Krivi unos prvog broja!", "GRESKA");
            }
            if (!double.TryParse(textBox2.Text, out drugi))
            {
                MessageBox.Show("Krivi unos drugog broja!", "GRESKA");
            }
            rez = prvi - drugi;
            rezultat_label.Text = rez.ToString();
        }

        private void pomnozi_button_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out prvi))
            {
                MessageBox.Show("Krivi unos prvog broja!", "GRESKA");
            }
            if (!double.TryParse(textBox2.Text, out drugi))
            {
                MessageBox.Show("Krivi unos drugog broja!", "GRESKA");
            }
            rez = prvi * drugi;
            rezultat_label.Text = rez.ToString();
        }

        private void podijeli_button_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out prvi))
            {
                MessageBox.Show("Krivi unos prvog broja!", "GRESKA");
            }
            if (!double.TryParse(textBox2.Text, out drugi))
            {
                MessageBox.Show("Krivi unos drugog broja!", "GRESKA");
            }
            rez = prvi / drugi;
            rezultat_label.Text = rez.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox3.Text, out treci))
            {
                MessageBox.Show("Krivi unos treceg operanda!", "GRESKA");
            }
            rez = Math.Sin(treci);
            label3.Text = rez.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox3.Text, out treci))
            {
                MessageBox.Show("Krivi unos treceg operanda!", "GRESKA");
            }
            rez = Math.Cos(treci);
            label3.Text = rez.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox3.Text, out treci))
            {
                MessageBox.Show("Krivi unos treceg operanda!", "GRESKA");
            }
            rez = Math.Sqrt(treci);
            label3.Text = rez.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox3.Text, out treci))
            {
                MessageBox.Show("Krivi unos treceg operanda!", "GRESKA");
            }
            rez = Math.Exp(treci);
            label3.Text = rez.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox3.Text, out treci))
            {
                MessageBox.Show("Krivi unos treceg operanda!", "GRESKA");
            }
            rez = Math.Log(treci);
            label3.Text = rez.ToString();
        }
    }
}
